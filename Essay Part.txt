1.What is the difference between a comparison operator and an assignment operator?

Assignment operator is used to assign the result of the expression on the right side of the operator to the variable on the left side,  
Comparison operator is used to check whether the two expressions on both sides are equal or not. It returns true of they are equal, and false if they are not.

2.What is a Boolean? What does it represent?

Boolean refers to a system of logical thought that is used to create true/false statements. A Boolean value expresses a truth value (which can be either true or false). 
Boolean expressions use the operators AND, OR, XOR and NOT to compare values and return a true or false result.

3.How is the += operator different than + operator?

The += assignment operator adds a value to a variable while = assignment operator assigns a value to a variable.