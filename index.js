console.log ("connected");

let a = 10;
let b = 20;

// (+=)
/* console.log (a > b);    //false
console.log (a += 30);  //40
console.log (a > b);    //true
console.log (a >= b);   //true
console.log (a <= b);   //false
console.log (a !== b);  //true
 */

// (-=)
/* console.log (a > b);    //false
console.log (a -= 30);  //-20
console.log (a > b);    //false
console.log (a >= b);   //false
console.log (a <= b);   //true
console.log (a !== b);  //true */

// (*=)
/* console.log (a > b);    //false
console.log (a *= 30);  //300
console.log (a > b);    //true
console.log (a >= b);   //true
console.log (a <= b);   //false
console.log (a !== b);  //true */

// (/=)
console.log (a > b);    //false
console.log (a /= 30);  //0.3333333333333333
console.log (a > b);    //false
console.log (a >= b);   //false
console.log (a <= b);   //true
console.log (a !== b);  //true
